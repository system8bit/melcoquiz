﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace melcoquiz {
	class Program {
		class Dish : ICloneable {
			public Dish() {
			}

			public Dish(Dish d) {
				this.count = d.count;
				this.left = d.left;
				this.right = d.right;
			}

			public Dish left = null;
			public Dish right = null;

			public int count = 0;



			public Dish Clone() {
				Dish dish = new Dish();
				if (this.left != null) {
					dish.left = this.left.Clone();
				}
				else {
					dish.left = null;
				}

				if (this.right != null) {
					dish.right = this.right.Clone();
				}
				else {
					dish.right = null;
				}

				dish.count = this.count;
				return dish;
				
			}

			object ICloneable.Clone() {
				throw new NotImplementedException();
			}
		}

		static void Main(string[] args) {
			string cipheString = File.ReadAllText("../../cipher.txt").Replace("\r\n", "");
			Dish a = new Dish();
			Dish b = new Dish();
			Dish c = new Dish();
			Dish d = new Dish();

			Stack<int> rec = new Stack<int>();
			string result = "";
			for (int i = 0; i < cipheString.Length; i++) {
				char cmd = cipheString[i];
				if (cmd >= 'a' && cmd <= 'z') {
					a.count += (cmd - 'a' + 1);
				}
				else if (cmd == '0') {
					a.count = 0;
					a.left = null;
					a.right = null;
				}
				else if (cmd == '+') {
					a.count += b.count;
				}
				else if (cmd == '-') {
					a.count -= b.count;
					a.count = a.count < 0 ? 0 : a.count;
				}
				else if (cmd == '*') {
					a.count = a.count * b.count;
				}
				else if (cmd == '/') {
					a.count = a.count / b.count;
				}
				else if (cmd == '(') {
					int end = cipheString.IndexOf(')', i);
					string word = '#' + cipheString.Substring(i + 1, end - i - 1);
					i = cipheString.IndexOf(word, 0);//見つかる前提
				}
				else if (cmd == '[') {
					int end = cipheString.IndexOf(']', i);
					if (a.count == 0) {
						i = end;
					}
					else {
						string word = '#' + cipheString.Substring(i + 1, end - i - 1);
						int found = cipheString.IndexOf(word, i);
						i = cipheString.IndexOf(word, 0);//見つかる前提
					}

				}
				else if (cmd == '{') {
					int end = cipheString.IndexOf('}', i);
					rec.Push(end);
					string word = '#' + cipheString.Substring(i + 1, end - i - 1);
					i = cipheString.IndexOf(word, 0);//見つかる前提
				}
				else if (cmd == '^') {
					i = rec.Pop();
				}
				else if (cmd == '=') {
					char first = char.ToLower(cipheString[i + 1]);
					char second = char.ToLower(cipheString[i + 2]);

					if (first == 'a') {
						if (second == 'b') {
							a = b.Clone() ;
						}
						else if (second == 'c') {
							a = c.Clone();
						}
						else if (second == 'd') {
							a = d.Clone();
						}
					}
					else if (first == 'b') {
						if (second == 'a') {
							b = a.Clone();
						}
						else if (second == 'c') {
							b = c.Clone();
						}
						else if (second == 'd') {
							b = d.Clone();
						}
					}
					else if (first == 'c') {
						if (second == 'a') {
							c = a.Clone();
						}
						else if (second == 'b') {
							c = b.Clone();
						}
						else if (second == 'd') {
							c = d.Clone();
						}
					}
					else if (first == 'd') {
						if (second == 'a') {
							d = a.Clone();
						}
						else if (second == 'b') {
							d = b.Clone();
						}
						else if (second == 'c') {
							d = c.Clone();
						}
					}

					i += 2;

				}
				else if (cmd == '.') {
					Dish newDish = new Dish();
					if (a.count == 0 && a.left == null && a.right == null) {
						newDish.left = null;
					}
					else {
						newDish.left = a;
					}

					if (b.count == 0 && b.left == null && b.right == null) {
						newDish.right = null;
					}
					else {
						newDish.right = b.Clone();
					}
					newDish.count = 0;
					a = newDish;
				}
				else if (cmd == ',') {
					a.count = 0;
					if (a.right != null || a.left != null) {
						a.count += 1;
					}
					a.right = null;
					a.left = null;
				}
				else if (cmd == '<') {
					a = a.left;
					if (a == null) {
						a = new Dish();
					}
				}
				else if (cmd == '>') {
					a = a.right;
					if (a == null) {
						a = new Dish();
					}
				}
				else if (cmd == '!') {
					char tmp = (char)a.count;
					result += tmp;
					Console.Write(tmp);
				}
			}

			File.WriteAllText("result.txt", result);
		}
	}
}
