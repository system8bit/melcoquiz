﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Threading;

namespace melcoquiz2 {
	public partial class Form1 : Form {
		public Form1() {
			InitializeComponent();
		}

		Thread mainThread = null;

		private void button1_Click(object sender, EventArgs e) {
			string mazeString = File.ReadAllText("../../maze.txt");

			AStart aStar = new AStart(mazeString);
			textBox1.Text = mazeString;

			if (mainThread != null) {
				mainThread.Abort();
			}

			textBox2.Text = aStar.resolve();

			string result = textBox2.Text;

			string cipher = "";


			char old = char.MaxValue;
			foreach (var s in result) {
				if (old != s) {
					cipher += '0';
					char tmp = s;
					while (tmp - 26 > 0) {
						cipher += 'z';
						tmp -= (char)26;
					}
					if (tmp > 0) {
						cipher += (char)('a' + tmp);
					}
				}

				cipher += '?';
				old = s;
			}
			
		}

		class AStart {
			Node[,] maze;
			Node startNode;
			Node goalNode;

			int W;
			int H;

			private int[] dx = new[] { 0, 1, 0, -1 };
			private int[] dy = new[] { 1, 0, -1, 0 };

			List<Node> unfixedNode = new List<Node>();

			public AStart(string mazeString) {
				var lines = mazeString.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);
				W = lines[0].Length;
				H = lines.Length;
				maze = new Node[H, W];
				bool firstGoal = false;
				for (int i = 0; i < lines.Length; i++) {
					for (int j = 0; j < lines[i].Length; j++) {
						maze[i, j] = new Node(lines[i][j], j, i);

						if (maze[i, j].IsStart) {
							startNode = maze[i, j];
						}
						if (maze[i, j].IsGoal && !firstGoal) {
							goalNode = maze[i, j];
							firstGoal = true;
						}
					}
				}

				startNode.GS = 0;
				startNode.FS = startNode.GS + getHs(startNode);
				unfixedNode.Add(startNode);

				
			}

			public string resolve() {
				nextStep();

				Stack<Node> routeStack = new Stack<Node>();
				routeStack.Push(goalNode);
				Node prev = goalNode.PrevNode;
				while (prev != null) {
					routeStack.Push(prev);
					prev.IsRoute = true;
					prev = prev.PrevNode;
				}

				string result = "";

				Node src = routeStack.Pop();
				Node dest = routeStack.Pop();

				while (dest != null) {
					result += getDirection(src, dest);

					src = dest;
					if (routeStack.Count > 0) {
						dest = routeStack.Pop();
					}
					else {
						dest = null;
					}

				}

				return result;
			}



			private char getDirection(Node src, Node dest) {
				if (src.Y == dest.Y) {
					if (src.X - dest.X > 0) {
						return 'L';
					}
					else {
						return 'R';
					}
				}
				else {
					if (src.Y - dest.Y > 0) {
						return 'U';
					}
					else {
						return 'D';
					}
				}
			}

			private int getHs(Node node) {
				return Math.Abs(node.X - goalNode.X) + Math.Abs(node.Y - goalNode.Y);

			}

			private void nextStep() {
				int minScore = Int16.MaxValue;
				Node u = null;

				foreach (var node in unfixedNode) {
					if (node.IsDone) {
						continue;
					}

					if (node.FS < minScore) {
						minScore = node.FS;
						u = node;
					}
				}

				if (u == null) {
					return;
				}

				unfixedNode.Remove(u);
				u.IsDone = true;

				if (u.IsGoal) {
					return;
				}

				for (int i = 0; i < dx.Length; i++) {
					if (u.Y + dy[i] < 0 || u.Y + dy[i] >= H || u.X + dx[i] >= W || u.X + dx[i] < 0) {
						continue;
					}

					Node v = maze[u.Y + dy[i], u.X + dx[i]];

					if (v.IsDone || v.IsWall) {
						continue;
					}

					if (u.GS + 1 + getHs(v) < v.FS) {
						v.GS = u.GS + 1;
						v.FS = v.GS + getHs(v);
						v.PrevNode = u;

						if (unfixedNode.IndexOf(v) == -1) {
							unfixedNode.Add(v);
						}
					}


				}

				nextStep();
			}
		}


		class Node {
			public int FS { get; set; }
			public int GS { get; set; }

			public bool IsDone { get; set; }

			public Node PrevNode { get; set; }

			public bool IsWall { get; private set; }
			public bool IsGoal { get; private set; }

			public bool IsStart { get; private set; }


			public bool IsRoute { get; set; }

			public int X { get; private set; }
			public int Y { get; private set; }

			public Node(char symbol, int x, int y) {
				if (symbol == '.') {
					if (x == 0 || y == 0) {
						IsGoal = true;
					}
				}
				else if (symbol == '#') {
					IsWall = true;
				}
				else if (symbol == '@') {
					IsStart = true;
				}
				else if (symbol == '?') {

				}

				this.X = x;
				this.Y = y;

				FS = int.MaxValue;
				GS = int.MaxValue;
			}
		}
	}
}
